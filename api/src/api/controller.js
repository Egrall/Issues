exports = module.exports = ($repository, $result) =>{
    return model => {
        return {
            findAll: (req, res) => {
                $repository(model).findAll()
                .then(data => {
                    res.json($result.success(data));
                })
                .catch(error => {
                    res.json($result.error(error));
                });
            },
            findById: (req, res) => {
                $repository(model).findById(req.params.id)
                .then(data => {
                    if(data){
                        res.json($result.success(data));
                    } else {
                        res.json($result.error('Element not found'));
                    }
                })
                .catch(error => {
                    res.json($result.error(error));
                });
            },
            create: (req, res) => {
                $repository(model).create(req.body)
                .then(data => {
                    res.json($result.success(data));
                })
                .catch(error => {
                    res.json($result.error(error));
                });
            },
            update: (req, res) => {
                $repository(model).update(req.params.id, req.body)
                .then(data => {
                    res.json($result.success(data));
                })
                .catch(error => {
                    res.json($result.error(error));
                });
            },
            delete:  (req, res) => {
                $repository(model).delete(req.params.id)
                .then(data => {
                    res.json($result.success(data));
                })
                .catch(error => {
                    res.json($result.error(error));
                });
            }
        };
    };
};


// Decorators
exports['@require'] = ['api/repository', 'api/result'];
exports['@singleton'] = true;