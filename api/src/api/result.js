exports = module.exports = () => {
    return new Result();
};

exports['@singleton'] = true;

class Result {

    constructor(){}

    error(error){
        let message = typeof error === 'string' ? error : 
        (error.constructor === Error ? error.message : 
            (error.message ? error.message : 'Uknown error')
        );
        return this.format('error', error, null, message);
    }

    success (data) {
        return this.format('success', data, null, null);
    }

    fail (data) {
        return this.format('fail', data, null, null);
    }

    session(){
        return this.format('session', null, null, null);
    }
    /**
     * Create an object with format to set the result
     * @param {string} status Status type (error, fail, success)
     * @param {string} code Error codes
     * @param {object} data Data to set as result
     * @param {string} message Message on error/session/accesss
     */
    format (status, data, code, message) {
        return {status: status, data: data, code: code, message: message};
    }
}