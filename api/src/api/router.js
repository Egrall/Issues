exports = module.exports = ($controller, $express) => {
    let IoC = require('electrolyte');
    let router = $express.Router();

    router.get(
        '/:model',
        loadModel('findAll')
    );
    router.post(
        '/:model',
        loadModel('create')
    );
    router.get(
        '/:model/:id',
        loadModel('findById')
    );
    router.put(
        '/:model/:id',
        loadModel('update')
    );
    router.delete(
        '/:model/:id',
        loadModel('delete')
    );
    
    return router;
    
    function loadModel(method) {
        return (req, res) => {
            IoC.create(`${req.params.model}/model`)
            .then(model => {
                return $controller(model)[method](req, res);
            })
            .catch(error =>{
                return res.status(500).send({message: 'Model not  found', error: error});
            });
        };
    }


};

exports['@singleton'] = true;
exports['@require'] = ['api/controller', 'express'];