exports = module.exports = ($repository, $result) => {
    return {
        getBansByProject: (req, res) => {
            $repository.getBansByProject(req.params.project)
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        }
    };
}

exports['@require'] = ['bans/repository','api/result'];