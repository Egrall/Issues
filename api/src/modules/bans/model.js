exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        project: { type: $sequelize.UUID, allowNull: false },
        user: { type: $sequelize.UUID, allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('ban', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt'] }}
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];