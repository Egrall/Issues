exports = module.exports = ($model, $database) => {
    let $connection = $database.connect();
    return {
        getBansByProject: (project) => {
            return new Promise((resolve, reject) => {
                $model.findAll({where: {project: project}})
                .then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    }
}

exports['@require'] = ['bans/model','common/database'];