exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/byProject/:project', $controller.getBansByProject);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','bans/controller'];