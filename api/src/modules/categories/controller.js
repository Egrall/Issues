exports = module.exports = ($repository, $result) =>{
    return {
        getCategoriesByProject: (req, res) => {
            $repository.getCategoriesByProject(req.params.project)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        },
        deleteCategories: (req, res) => {
            $repository.deleteCategories(req.params.id)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        }
    };
}

exports['@require'] = ['categories/repository', 'api/result'];