exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        name: { type: $sequelize.STRING, allowNull: false },
        description: { type: $sequelize.STRING, allowNull: false },
        priority: { type: $sequelize.ENUM('low','medium','high'), allowNull: false },
        project: { type: $sequelize.UUID, allowNull: false },
        maxVideos: { type: $sequelize.INTEGER, defaultValue: 5, allowNull: false },
        maxAudios: { type: $sequelize.INTEGER, defaultValue: 5, allowNull: false },
        maxImages: { type: $sequelize.INTEGER, defaultValue: 5, allowNull: false },
        enableSetPriorities: { type: $sequelize.BOOLEAN, allowNull: false },
        enableSetGeolocation: { type: $sequelize.BOOLEAN, allowNull: false },
        enableSetState: { type: $sequelize.BOOLEAN, allowNull: false },
        enableAddText: { type: $sequelize.BOOLEAN, allowNull: false },
        enableEditTitle: { type: $sequelize.BOOLEAN, allowNull: false },
        user: { type: $sequelize.UUID, allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('categories', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];