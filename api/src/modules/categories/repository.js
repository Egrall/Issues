exports = module.exports = ($model, $database, $issues, $async) => {
    let $connection = $database.connect();
    let response = [];
    return {
        getCategoriesByProject: (project) => {
            return new Promise((resolve, reject) => {
                $model.findAll({where: {project: project}})
                .then(data => {
                    $async.each(data, findCategoriesByProject,  (err) => {
                        if(err) {
                            reject(err);
                        } else {
                            let aux = [];
                            response.map(item => {
                                aux.push(item);                                
                            });
                            response = [];
                            resolve(aux);
                        }
                    });
                })
                .catch(error => {
                   reject(error); 
                });
            });
        },
        deleteCategories: (id) => {
            return new Promise((resolve, reject) => {
                $connection.sync().then(() => {
                    $issues.findAll({where: {category: id}})
                    .then(data => {
                        $async.each(data, deleteRelatedIssues, err =>{
                            if(err) {
                                reject(err);
                            } else {
                                $model.findById(id)
                                .then(result => {
                                    return result.destroy()
                                    .then(self => {
                                        let updated = self.get({plain: true, attributes: ['id','deletedAt']});
                                        resolve({id: updated.id, deletedAt: updated.deletedAt});
                                    });
                                });
                            }
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                })
                .catch(error => {
                    reject(error);
                });
            });
        } 
    };

    function findCategoriesByProject(category, callback){
        let aux = category.toJSON();
        $issues.findAll({where: {category: category.id}})
        .then(data => {
            aux.totalIssues = data.length;
            response.push(aux);
            callback();
        });
    }

    function deleteRelatedIssues(issue, callack) {
        issue.destroy()
        .then(() => {
            callack();
        });
    }

}

exports['@require'] = ['categories/model','common/database', 'issues/model', 'async'];