exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/categoriesByProject/:project', $controller.getCategoriesByProject);
    router.delete('/deleteCategories/:id', $controller.deleteCategories);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','categories/controller'];