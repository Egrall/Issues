exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        category: { type: $sequelize.UUID, allowNull: false },
        name: { type: $sequelize.STRING, allowNull: false },
        description: { type: $sequelize.STRING, allowNull: false },
        required: { type: $sequelize.BOOLEAN, allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('categoryextra', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];