exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        value: { type: $sequelize.DATE, allowNull: false },
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('extramoments', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database'];