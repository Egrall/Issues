exports = module.exports = ($repository, $result) =>{
    return {
        getIssuesByProject: (req, res) => {
            $repository.getIssuesByProject(req.params.project)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        }
    };
}

exports['@require'] = ['issues/repository', 'api/result'];