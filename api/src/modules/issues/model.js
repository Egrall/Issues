exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        issueId: {type: $sequelize.INTEGER, unique: true, autoIncrement: true},
        user: { type: $sequelize.UUID, allowNull: false },
        title: { type: $sequelize.STRING, allowNull: false },
        project: { type: $sequelize.UUID, allowNull: false},
        category: { type: $sequelize.UUID, allowNull: false },
        status: { type: $sequelize.ENUM('open','resolved','rejected'), allowNull: false },
        priority: { type: $sequelize.ENUM('low','medium','high'), allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('issue', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        scopes: {
            issues: {attributes: { exclude: ['updatedAt','deletedAt'] }}
        },
        paranoid: true
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];