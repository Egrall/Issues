exports = module.exports = ($model, $database, $async, $projects, $categories) => {
    let $connection = $database.connect();
    let response = [];
    return {
        getIssuesByProject: (project) => {
            return new Promise((resolve, reject) => {
                $model.scope('issues').findAll({where: {project: project}})
                .then(data => {
                    $async.each(data, populateIssues, err => {
                        if(err) {
                            reject(err);
                        } else {
                            let aux = [];
                            response.map(item => {
                                aux.push(item);                                
                            });
                            response = [];
                            resolve(aux);
                        }
                    });
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    };

    function populateIssues(issue, callback) {
        let aux = issue.toJSON();
        $projects.findById(issue.project)
        .then(project => {
            aux.project = project;
            $categories.findById(issue.category)
            .then(category => {
                aux.category = category;
                response.push(aux);
                callback();
            });
        });
    }
}

exports['@require'] = ['issues/model','common/database','async','projects/model','categories/model'];