exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/issuesByProject/:project', $controller.getIssuesByProject);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','issues/controller'];