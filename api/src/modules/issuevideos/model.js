exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        user: { type: $sequelize.UUID, allowNull: false },
        issue: { type: $sequelize.UUID, allowNull: false },
        url: { type: $sequelize.STRING, allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('issuevideo', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];