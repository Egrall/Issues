exports = module.exports = ($repository, $result, $settings, $fs, $path) => {
    return {
        projectsByUser: (req, res) => {
            $repository.projectsByUser(req.params.user)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        },
        uploadImage: (req, res) => {
            let fileName = 'File not provided';
            if(req.files) {
                let filePath = req.files.picture.path;
                console.log(filePath);
                let fileSplit = filePath.split('\\');
                fileName = fileSplit[fileSplit.length-1];
                let extSplit = fileName.split('.');
                let fileExt = extSplit[1].toUpperCase();
                if(fileExt == 'PNG' || fileExt == 'JPG' || fileExt == 'JPEG' ||fileExt == 'GIF'){
                    $repository.uploadImage(req.params.id, fileName)
                    .then(result => {
                        let response = $result.success(result);
                        if(result.deletedPicture){
                            let fp = filePath.replace(fileName,result.deletedPicture);
                            $fs.unlink(fp, (err) => {
                                if(err){
                                    response.message = 'Updated correctly and previous file was not deleted';
                                }else{
                                    response.message = 'Updated correctly';
                                }
                            });
                        }
                        response.image = fileName;
                        res.json(response);
                    })
                    .catch(error => {
                        res.json($result.error(error));
                    });
                } else {
                    $fs.unlink(filePath, (err) =>{
                        if(err){
                            res.json($result.error('Invalid extension and file was not deleted'));
                        }else{
                            res.json($result.error('Invalid extension'));
                        }
                    });
                }
            }
        },
        getImage: (req, res) => {
            const config = $settings.get();
            const __cwd = process.cwd();
            let fileName = req.params.image;
            let pathFile = `${__cwd}/${config.modules}/projects/uploads/`;
            $fs.exists(pathFile + fileName, function(exists){
                if(exists){
                    res.sendFile($path.resolve(pathFile + fileName));
                }else{
                    $fs.exists(pathFile + 'unknown.png', function(exists){
                        if(exists){
                            res.sendFile($path.resolve(pathFile + 'unknown.png'));
                        } else {
                            res.json($result.error('Picture not found'));
                        }
                    });
                }
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['projects/repository', 'api/result', 'base/settings', 'fs','path'];