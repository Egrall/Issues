exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        name: { type: $sequelize.STRING, allowNull: false },
        description: { type: $sequelize.STRING, allowNull: false },
        latitude: { type: $sequelize.DOUBLE, allowNull: false },
        longitude: { type: $sequelize.DOUBLE, allowNull: false },
        picture: { type: $sequelize.STRING, allowNull: true },
        visibility: { type: $sequelize.ENUM('private','public','protected'), allowNull: false},
        user: {type: $sequelize.UUID, allowNull: false}
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (project, options) {
            return new Promise ((resolve, reject) => {
                if(project.id)
                    delete project.id;
                resolve(project);
            });
        }
    };

    const Model = $connection.define('project', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database','uuid/v4'];