exports = module.exports = ($model, $database) => {
    let connection = $database.connect();
    return {
        projectsByUser: user => $model.findAll({where: {user: user}}),
        uploadImage: (id, ruta) => {
            return new Promise((resolve, reject) => {
                connection.sync().then(() => {
                    let response = {};
                    return $model.findById(id)
                    .then(result => {
                        console.log(result);
                        if(result.picture)
                            response.deletedPicture = result.picture;
                        return result.update({picture: ruta})
                        .then(self => {
                            let updated = self.get({plain: true, attributes: ['id','picture']});
                            console.log(updated);
                            response.id = updated.id;
                            resolve(response);
                        })
                        .catch(error => {
                            reject(error);
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    };
};

exports['@require'] = ['projects/model', 'common/database'];