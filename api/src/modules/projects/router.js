exports = module.exports = ($express, $controller, $multipart, $settings) => {
    const config = $settings.get();
    const __cwd = process.cwd();
    let md_upload = $multipart({uploadDir: `${__cwd}/${config.modules}/projects/uploads`});    
    let router = $express.Router();

    router.get('/projectsByUser/:user',$controller.projectsByUser);
    router.post('/uploadImage/:id',md_upload,$controller.uploadImage);
    router.get('/getImage/:image',$controller.getImage);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','projects/controller','connect-multiparty','base/settings'];