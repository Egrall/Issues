exports = module.exports = ($repository, $result) => {
    return {
        getUsers: (req, res) => {
            $repository.getUsers(req.params.project)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        },
        getProjects: (req, res) => {
            $repository.getProjects(req.params.user)
            .then(data => {
                res.json($result.success(data));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['projectusers/repository', 'api/result'];