exports = module.exports = ($sequelize, $database, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        user: { type: $sequelize.UUID, allowNull: false },
        project: { type: $sequelize.UUID, allowNull: false },
        isAdmin: { type: $sequelize.BOOLEAN, allowNull: false },
        isManager: { type: $sequelize.BOOLEAN, allowNull: false },
        isUser: { type: $sequelize.BOOLEAN, allowNull: false }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('projectuser', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt'] }}
    });
    
    return Model;
};

exports['@require'] = ['sequelize', 'common/database','uuid/v4'];