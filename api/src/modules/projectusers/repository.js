exports = module.exports = ($model, $database, $users, $projects, $issues, $async) => {
    let connection = $database.connect();
    let response = [];    
    return {
        getUsers: project =>{
            return new Promise((resolve, reject) => {
                $model.findAll({where:{project:project}})
                .then(data => {
                    if(data.length > 0) {
                        $async.each(data, findProjectUsers, err => {
                            if(err) {
                                reject(err);
                            } else {
                                let aux = [];
                                response.map(item => {
                                    aux.push(item);
                                });
                                response = [];
                                resolve(aux);
                            }
                        });
                    }
                    else{
                        resolve(data);
                    }
                })
                .catch(error => {
                    reject(error);
                });
            });
        },
        getProjects: user => {
            return new Promise((resolve, reject) => {
                $model.findAll({where:{user:user}})
                .then(data => {
                    if(data.length > 0) {
                        $async.each(data, findUserProjects, err => {
                            if (err) {
                                reject(err);
                            } else {
                                let aux = []
                                response.map(items => {
                                    aux.push(items);
                                });
                                response = [];
                                resolve(aux);
                            }
                        });
                    } else {
                        resolve(response);
                    }
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    };
    function findProjectUsers(projectUser, callback) {
        $users.findById(projectUser.user)
        .then(item => {
            projectUser.user = item;
            response.push(projectUser);
            callback();
        })
    }
    function findUserProjects(projectUser, callback) {
        $projects.findById(projectUser.project)
        .then(item => {
            if(item) {
                projectUser.project = item.toJSON();
                $issues.findAll({where: {project: item.id}})
                .then(data => {
                    projectUser.project.totalIssues = data.length;
                    let count = 0;
                    for(let i = 0; i<data.length; i++) {
                        if(data[i].status==='open')
                            count++;
                    }
                    projectUser.project.openIssues = count;
                    response.push(projectUser);
                    callback();
                })
                .catch(error => {
                    callback();
                });
            } else{
                callback();
            }
        })
        .catch(error => {
            callback();
        });
    }
};

exports['@require'] = ['projectusers/model','common/database','users/model','projects/model','issues/model','async'];