exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/getUsers/:project',$controller.getUsers);
    router.get('/getProjects/:user',$controller.getProjects);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','projectusers/controller'];