exports = module.exports = ($sequelize, $database, $bcrypt, $uuid4) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.UUID, primaryKey:true },
        name: { type: $sequelize.STRING, allowNull: false },
        lastName: { type: $sequelize.STRING, allowNull: false },
        email: { type: $sequelize.STRING, allowNull: false, unique: true },
        password: {type: $sequelize.STRING, allowNull: false },
        username: { type: $sequelize.STRING, allowNull: false, unique: true },
        phone: { type: $sequelize.STRING , allowNull: true },
        organization: { type: $sequelize.STRING, allowNull: true },
        city: { type: $sequelize.STRING, allowNull: true },
        picture: { type: $sequelize.STRING, allowNull: true }
    };

    const hooks = {
        beforeCreate: function (item, options) {
            item.id = $uuid4();
            return new Promise ((resolve, reject) => {
                $bcrypt.hash(item.password, null, null, function(err, hash){
                    if(err){
                        reject(error);
                    }else{
                        item.password = hash;
                        resolve(item);
                    }
                });
            });
        },
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                if(item.password)
                    $bcrypt.hash(item.password, null, null, function(err, hash){
                        if(err){
                            reject(error);
                        }else{
                            item.password = hash;
                            resolve(item);
                        }
                    });
                else{
                    resolve(item);
                }
            });
        }
    };

    const Model = $connection.define('user', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['password','updatedAt','createdAt','deletedAt'] }},
        scopes: {
            login: { attributes:['id','password', 'username'] }
        },
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database', 'bcrypt-nodejs','uuid/v4'];