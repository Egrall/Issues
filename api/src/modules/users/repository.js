exports = module.exports = ($model, $bcrypt, $authenticate, $database) => {
    let connection = $database.connect();
    return {
        login: (username, password) => {
            return new Promise((resolve, reject) => {
                let user;
                if(!username) reject('Username not provided');
                if(!password) reject('Password not provided');

                $model.scope('login').findOne({where: {username: username}, raw: true})
                .then(found => {
                    if (found) {
                        user = found;
                        $bcrypt.compare(password, user.password, (err, check) => {
                            if(check){
                                $authenticate.sign(user)
                                .then(token => {
                                    delete user.password;
                                    delete user.username;
                                    resolve({token: token, user: user});
                                })
                                .catch(error => reject(error));
                            } else{
                                reject('Invalid credentials');
                            }
                        });
                    } else reject('User not found');
                });
            });
        },
        validateEmail: email => {
            return new Promise((resolve, reject) => {
                let response = {};
                if(!email) reject('Data not provided');
                $model.findOne({where: {email: email}, raw: true})
                .then(found => {
                    if(found){
                        response.id = found.id;
                        response.email = true;
                    }
                    resolve(response);
                })
                .catch(error =>{
                    reject(error);
                });
            });
        },
        validateUsername: username => {
            return new Promise((resolve, reject) => {
                let response = {};
                if(!username) reject('Data not provided');
                $model.findOne({where: {username: username}, raw: true})
                .then(found => {
                    if(found){
                        response.id = found.id;
                        response.username = true;
                    }
                    resolve(response);
                })
                .catch(error =>{
                    reject(error);
                });
            });
        },
        uploadImage: (id, ruta) => {
            return new Promise((resolve, reject) => {
                connection.sync().then(() => {
                    let response = {};
                    return $model.findById(id)
                    .then(result => {
                        response.deletedPicture = result.picture;
                        return result.update({picture: ruta})
                        .then(self => {
                            let updated = self.get({plain: true, attributes: ['id']});
                            response.id = updated.id;
                            resolve(response);
                        })
                        .catch(error => {
                            reject(error);
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                })
                .catch(error => {
                    reject(error);
                });
            });
        },
        getUserByUsername: (username) => {
            return new Promise((resolve, reject)=>{
                $model.findOne({where: {username: username}})
                .then(item => {
                    if(item) {
                        resolve(item);
                    } else {
                        resolve(null);
                    }
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    };
};

exports['@require'] = ['users/model','bcrypt-nodejs','api/authenticate', 'common/database'];