exports = module.exports = ($express, $controller, $multipart, $settings) => {
    const config = $settings.get();
    const __cwd = process.cwd();
    let md_upload = $multipart({uploadDir: `${__cwd}/${config.modules}/users/uploads`});    
    let router = $express.Router();

    router.post('/login', $controller.login);
    router.post('/validateUsername',$controller.validateUsername);
    router.post('/validateEmail',$controller.validateEmail);
    router.post('/uploadImage/:id',md_upload,$controller.uploadImage);
    router.get('/getImage/:image',$controller.getImage);
    router.get('/byUsername/:username', $controller.getUserByUsername);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','users/controller','connect-multiparty','base/settings'];