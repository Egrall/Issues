import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import { Component, OnInit, DoCheck } from '@angular/core';
import { GlobalService } from './services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UsersService, AuthService]
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(
    private userService: UsersService,
    private GLOBAL: GlobalService,
    private auth: AuthService
  ) {
  }
  ngOnInit() {
  }
}
