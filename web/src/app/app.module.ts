import { ProjectsService } from './services/projects.service';
import { CurrentUserResolve } from './services/currentUser.resolve';
import { GlobalService } from './services/global.service';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { ProjectsModule } from './modules/projects/projects.module';
import { DashboardMenuComponent, DashboardSubMenuComponent } from './components/dashboard/dashboard.menu.component';
import { AppRoutes } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import {DataTableModule, ButtonModule, InputTextModule, TabViewModule, RadioButtonModule, GMapModule,
  InputTextareaModule, CheckboxModule, FileUploadModule, PanelModule, ConfirmDialogModule, GrowlModule,
  TooltipModule, MessagesModule, TieredMenuModule} from 'primeng/primeng';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Error404Component } from './components/404/404.component';
import { LoggedGuard } from './services/logged.guard';
import { AuthService } from './services/auth.service';
import { AccountEditComponent } from './components/account-edit/account-edit.component';
import { UsersService } from './services/users.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    Error404Component,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    DashboardMenuComponent,
    DashboardSubMenuComponent,
    AccountEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutes,
    InputTextModule,
    ButtonModule,
    CheckboxModule,
    BrowserAnimationsModule,
    ProjectsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    MessagesModule,
    PanelModule,
    FileUploadModule,
    TieredMenuModule
  ],
  providers: [LoggedGuard, AuthService, UsersService, GlobalService, CurrentUserResolve],
  bootstrap: [AppComponent]
})
export class AppModule { }
