import { CurrentUserResolve } from './services/currentUser.resolve';
import { LoggedGuard } from './services/logged.guard';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProjectsModule } from './modules/projects/projects.module';

import { Error404Component } from './components/404/404.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LandingComponent } from './components/landing/landing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules} from '@angular/router';
import { AccountEditComponent } from './components/account-edit/account-edit.component';


export const routes: any = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'landing', component: LandingComponent, resolve: {currentUser: CurrentUserResolve}},
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent, resolve: {currentUser: CurrentUserResolve}},
  { path: 'dashboard', canActivate: [LoggedGuard], component: DashboardComponent,
    resolve: { currentUser: CurrentUserResolve },
    children: [
      { path: 'projects', loadChildren: getProjectsModule },
      { path: 'account', component: AccountEditComponent },
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo: '404' },
  { path: '404', component: Error404Component }
];

export function  getProjectsModule() {
  return ProjectsModule;
}

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule],
  providers: []
})

export class AppRoutes {}
