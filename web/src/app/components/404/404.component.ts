import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-404',
  templateUrl: './404.component.html',
  styleUrls: ['./404.component.css']
})
export class Error404Component implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {
    $('body').addClass('exception-body');
  }
  ngOnDestroy() {
    $('body').removeClass('exception-body');
  }
}
