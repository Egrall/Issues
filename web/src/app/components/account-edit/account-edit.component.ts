import { GlobalService } from './../../services/global.service';
import { Component, OnInit, ViewChildren, AfterViewInit } from '@angular/core';
import { fadeLateral } from './fadeLateral';
import { UsersService } from '../../services/users.service';
import { User } from '../../models/user.model';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { validarNumeros, passwordConfirming, emailValidator} from '../../validators/validator';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  animations: [fadeLateral],
  providers: [UsersService]
})

export class AccountEditComponent implements OnInit, AfterViewInit {
  @ViewChildren('first') first;
  public user: User;
  public accountForm: FormGroup;
  public emailUnique: Boolean;
  public url: string;
  constructor(
    private fb: FormBuilder,
    private userService: UsersService,
    public GLOBAL: GlobalService
  ) {
    this.url = this.GLOBAL.url;
    this.emailUnique = false;
    this.user = new User(null, null, null, null, null, null, null, null, null, null);
    this.accountForm = this.fb.group({
      lastName: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, emailValidator] ],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      city: [''],
      organization: ['']
    });
  }

  ngAfterViewInit() {
    this.first.first.nativeElement.focus();
  }

  fileChange(event) {
    if (event.target.files.length > 0) {
      this.userService.makeFileRequest(this.url + '/endpoint/users/uploadImage/' + this.user.id, [], event.target.files, 'picture')
      .then((result: any) => {
        this.GLOBAL.user.picture = result.image;
        this.user.picture = result.image;
      });
    }
  }

  resetEmailUnique() {
    this.emailUnique = false;
  }
  guardar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.userService.updateUser(this.user).subscribe(
      result => {
        if (result.status == 'success') {
          if (this.GLOBAL.user.id == result.data.id) {
            Object.assign( this.GLOBAL.user, this.user);
          }
        }else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  validarNum(code) {
    return validarNumeros(code);
  }

  ngOnInit() {
    Object.assign(this.user, this.GLOBAL.user);
  }
}
