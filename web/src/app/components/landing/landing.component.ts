import { fadeIn } from './../animation';
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import {MenuItem} from 'primeng/primeng';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
  animations: [fadeIn]
})

export class LandingComponent implements OnInit {
  public user: User;
  public isLogged: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService
  ) { }
  ngOnInit() {
    $('#menu-button').on('click', function(e) {
      $('#menu').toggleClass('overlay-menu');
      e.preventDefault();
    });
    $('#menu').find('a').on('click', function(e) {
        $('#menu').removeClass('overlay-menu');
    });
    this.isLogged = this.auth.isLogged();
    if (this.auth.isLogged()) {
      this.user = this.route.snapshot.data['currentUser'].data;
    }
  }

}
