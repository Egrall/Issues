import { Message } from 'primeng/primeng';
import { User } from './../../models/user.model';
import { fadeIn } from './../animation';
import { Component, OnInit, OnDestroy} from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  animations: [fadeIn],
  providers: [AuthService, UsersService]
})
export class LoginComponent implements OnInit, OnDestroy {
  public user: User;
  public loginForm: FormGroup;
  public status: String;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private userService: UsersService,
    private fb: FormBuilder,
    private GLOBAL: GlobalService
  ) {
    this.user = new User(null, null, null, null, null, null, null, null, null, null);
    this.loginForm = fb.group({
      password: ['', Validators.required],
      username: ['', Validators.required],
      remember: ['']
    });
  }

  login(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.auth.login(this.user).subscribe(
      result => {
        this.status = result.status;
        if (this.status == 'success') {
          localStorage.setItem('token', result.data.token);
          localStorage.setItem('currentID', result.data.user.id);
          this.router.navigate(['/dashboard']);
        }else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  ngOnInit() {
    $('body').addClass('login-body');
    $('#firstInput').focus();
  }
  ngOnDestroy() {
    $('body').removeClass('login-body');
  }

}
