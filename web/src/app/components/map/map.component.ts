import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { GoogleMapsService } from './map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  providers: [GoogleMapsService]
})

export class MapComponent implements OnInit{

  private active: any;
  private options: any;
  private searchLocationText = '';
  public map: google.maps.Map;
  public marker = [];
  private latValue:  number;
  private lngValue: number;
  private geocoder: google.maps.Geocoder;
  private geocoderStatus: google.maps.GeocoderStatus;

  @Output() lngChange = new EventEmitter();
  @Output() latChange = new EventEmitter();

  @Input() search = false;
  @Input() oneMark = true;
  @Input() currentPositionMark = true;

  @Input()
  get lat() {
    return this.latValue;
  }
  set lat(val) {
    this.latValue = val;
    this.latChange.emit(this.latValue);
  }
  @Input()
  get lng() {
    return this.lngValue;
  }
  set lng(val) {
    this.lngValue = val;
    this.lngChange.emit(this.lngValue);
  }

  constructor(private GMapService: GoogleMapsService) {
  }

  setMap(event) {
    this.map = event.map;
    let center;
    const addMapListener = google.maps.event.addDomListener;

    if (this.currentPositionMark) {
      if (window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(this.setCurrentPosition.bind(this));
      }
    } else {
      if (this.lat && this.lng) {
        this.map.setCenter(new google.maps.LatLng(this.lat, this.lng));
        this.marker.push(new google.maps.Marker({position: {lat: this.lat, lng: this.lng}, draggable: true}));
      }
    }

    addMapListener(this.map, 'idle', () => center = this.map.getCenter());
    addMapListener(window, 'resize', () => this.map.setCenter(center));
  }
  searchLoc() {
    if (this.searchLocationText !== '') {
      this.GMapService.getGeocoding(this.searchLocationText).subscribe(
        result => {
          // console.log('lat: ' + result.lat() + ', lng: ' + result.lng());
          this.map.setCenter(new google.maps.LatLng(result.lat(), result.lng()));
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  overlayDragEndHandler(event) {
    this.lat = event.overlay.getPosition().lat();
    this.lng = event.overlay.getPosition().lng();
  }
  clickHandler(event) {
    this.addMarker(event.latLng.lat(), event.latLng.lng());
  }
  addMarker(lat, lng) {
    GoogleMapsService.load().then((_mapsApi) => {
      this.lat = lat;
      this.lng = lng;
      if (this.oneMark) {
        this.marker = [];
      }
      this.marker.push(new google.maps.Marker({position: {lat: this.lat, lng: this.lng}, draggable: true}));
    });
  }
  setCurrentPosition(position) {
    GoogleMapsService.load().then((_mapsApi) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
      if (!(this.marker.length > 0)) {
        this.map.setCenter(new google.maps.LatLng(this.lat, this.lng));
        this.marker.push(new google.maps.Marker({position: {lat: this.lat, lng: this.lng}, draggable: true}));
      }
    });
  }
  ngOnInit() {
    this.options = {
      center: {lat: 25.7904657, lng: -108.985882},
      zoom: 12
    };
    GoogleMapsService.load().then((_mapsApi) => {
      this.active = true;
    });
  }
}
