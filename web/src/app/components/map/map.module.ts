import { MapComponent } from './map.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ButtonModule, GMapModule, InputTextModule } from 'primeng/primeng';
import {} from '@types/googlemaps';
@NgModule({
  imports: [
    FormsModule,
    ButtonModule,
    GMapModule,
    InputTextModule,
    CommonModule
  ],
  exports: [MapComponent],
  declarations: [
    MapComponent
  ],
  providers: [],
})
export class SMapModule { }
