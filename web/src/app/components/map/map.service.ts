import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const url = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAXjo4Hj6fDCoV5B1dXWFjkBfs72mQkPvc&callback=initMap';

@Injectable()
export class GoogleMapsService {
  private static promise;
  public static load() {
      // First time 'load' is called?
      if (!GoogleMapsService.promise) {

          // Make promise to load
          GoogleMapsService.promise = new Promise( resolve => {

              // Set callback for when google maps is loaded.
              window['initMap'] = (ev) => {
                resolve(window['google']['maps']);
              };

              const node = document.createElement('script');
              node.src = url;
              node.type = 'text/javascript';
              document.getElementsByTagName('head')[0].appendChild(node);
          });
      }
      // Always return promise. When 'load' is called many times, the promise is already resolved.
      return GoogleMapsService.promise;
  }
  getGeocoding(address: string) {
    return Observable.create(observer => {
        try {
            // at this point the variable google may be still undefined (google maps scripts still loading)
            // so load all the scripts, then...
            GoogleMapsService.load().then(() => {
                const geocoder = new google.maps.Geocoder();
                geocoder.geocode({ address }, (results, status) => {

                    if (status === google.maps.GeocoderStatus.OK) {
                        const place = results[0].geometry.location;
                        observer.next(place);
                        observer.complete();
                    } else {
                        console.error('Error - ', results, ' & Status - ', status);
                        if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                            observer.error('Address not found!');
                        }else {
                            observer.error(status);
                        }
                        observer.complete();
                    }
                });
            });
        } catch (error) {
            observer.error('error getGeocoding' + error);
            observer.complete();
        }

    });
  }
}
