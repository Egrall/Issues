import { User } from './../../models/user.model';
import { AuthService } from './../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeIn } from './../animation';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { validarNumeros, passwordConfirming, emailValidator} from '../../validators/validator';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  animations: [fadeIn],
  providers: [AuthService, UsersService]
})
export class SignupComponent implements OnInit, OnDestroy {
  public user: User;
  public repeatP: string;
  public registerForm: FormGroup;
  public emailUnique: Boolean;
  public usernameUnique: Boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private userService: UsersService,
    private fb: FormBuilder
  ) {
    this.emailUnique = false;
    this.usernameUnique = false;
    this.user = new User(null, null, null, null, null, null, null, null, null, null);
    this.registerForm = this.fb.group({
      lastName: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, emailValidator] ],
      username: ['', Validators.required],
      passwords: this.fb.group({
        password: ['', Validators.required],
        repeatP: ['', Validators.required]
      }, {validator: passwordConfirming}),
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
    });
  }

  resetEmailUnique() {
    this.emailUnique = false;
  }
  resetUsernameUnique() {
    this.usernameUnique = false;
  }
  registrar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.userService.validateUsername(this.user.username).subscribe(
      result => {
        if (result.status == 'success') {
          if (result.data) {
            this.usernameUnique = result.data.username;
          }
        } else {
          console.log(<any>result);
        }
        this.userService.validateEmail(this.user.email).subscribe(
          result => {
            if (result.status == 'success') {
              if (result.data) {
                this.emailUnique = result.data.email;
              }
            } else {
              console.log(<any>result);
            }
            this.auth.register(this.user).subscribe(
              result => {
                if (result.status == 'success') {
                  this.router.navigate(['/login']);
                }else {
                  console.log(<any>result);
                }
              },
              error => {
                console.log(<any>error);
              }
            );
          },
          error => {
            console.log(<any>error);
          }
        );
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  validarNum(code) {
    return validarNumeros(code);
  }
  ngOnInit() {
    $('body').addClass('login-body');
    $('#firstInput').focus();
  }
  ngOnDestroy() {
    $('body').removeClass('login-body');
  }


}
