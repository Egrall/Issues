import { Priority } from './priority.enum';

export class Category {
  constructor(
    public id: String,
    public name: String,
    public description: String,
    public priority: Priority,
    public project: String,
    public maxVideos: number = 5,
    public maxAudios: number = 5,
    public maxImages: number = 5,
    public enableSetPriorities: Boolean,
    public enableSetGeolocation: Boolean,
    public enableSetState: Boolean,
    public enableAddText: Boolean,
    public enableEditTitle: Boolean,
    public user: String,
  ) {
  }
}
