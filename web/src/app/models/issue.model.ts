import { Priority } from './priority.enum';
import { Status } from './status.enum';
export class Issue {
  constructor(
    public id: String,
    public issueId: Number,
    public user: String,
    public title: String,
    public project: String,
    public category: String,
    public status: Status,
    public priority: Priority
  ) {

  }
}
