import { Visibility } from './visibility.enum';
export class Project {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public latitude: number,
    public longitude: number,
    public picture: string,
    public visibility: Visibility,
    public user: string
  ) {

  }
}
