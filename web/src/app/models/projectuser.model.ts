export class ProjectUser {
  constructor(
    public id: string,
    public user: any,
    public project: any,
    public isAdmin: boolean,
    public isManager: boolean,
    public isUser: boolean
  ) {}
}
