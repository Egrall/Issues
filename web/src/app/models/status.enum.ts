export enum Status {
  open = 'open',
  resolved = 'resolved',
  rejected = 'rejected',
}
