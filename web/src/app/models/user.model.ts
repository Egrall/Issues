export class User {
  constructor(
    public id: string,
    public name: string,
    public lastName: string,
    public email: string,
    public password: string,
    public username: string,
    public phone: string,
    public organization: string,
    public city: string,
    public picture: string
  ) {
  }
}
