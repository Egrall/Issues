export enum Visibility {
  private = 'private',
  public = 'public',
  protected = 'protected',
}
