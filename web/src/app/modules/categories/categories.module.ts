import { GlobalService } from './../../services/global.service';
import { ProjectsResolve } from './../../services/projects.resolve';
import { EditComponent } from './edit/edit.component';
import { MainComponent } from './main/main.component';
import { SMapModule } from './../../components/map/map.module';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DataTableModule, ButtonModule, InputTextModule, TabViewModule, RadioButtonModule, GMapModule,
InputTextareaModule, CheckboxModule, FileUploadModule, PanelModule, ConfirmDialogModule, GrowlModule,
TooltipModule, ToggleButtonModule, ToggleButton, SpinnerModule} from 'primeng/primeng';
import { ProjectsService } from '../../services/projects.service';
import { CategoriesRoutes } from './categories.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ButtonModule,
    InputTextModule,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    InputTextareaModule,
    FileUploadModule,
    PanelModule,
    ConfirmDialogModule,
    SMapModule,
    GrowlModule,
    TooltipModule,
    ReactiveFormsModule,
    ToggleButtonModule,
    SpinnerModule,
    CategoriesRoutes
  ],
  declarations: [
    ListComponent,
    AddComponent,
    MainComponent,
    EditComponent
  ],
  exports: [MainComponent],
  providers: [ProjectsService, ProjectsResolve]
})
export class CategoriesModule { }
