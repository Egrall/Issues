import { GlobalService } from './../../../services/global.service';
import { Component, OnInit, AfterViewInit, NgZone, ViewChild} from '@angular/core';
import { fadeLateral } from '../animations/fadeLateral';
import { Router, ActivatedRoute } from '@angular/router';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { Category } from '../../../models/category.model';
import { Priority } from '../../../models/priority.enum';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  providers: [CategoryService],
  animations: [fadeLateral]
})

export class EditComponent implements OnInit, AfterViewInit {
  @ViewChild('first') first;
  title = 'Editar';
  public addCategory: FormGroup;
  private category: Category;
  private url: string;
  cPM = true;
  constructor(
    private fb: FormBuilder,
    private GLOBAL: GlobalService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.category = new Category(null, null, null, Priority.low, null, 5, 5, 5, true, true, true, true, true, null);
    this.url = GLOBAL.url;
    this.addCategory = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      enableSetPriorities: ['', Validators.required],
      enableSetGeolocation: ['', Validators.required],
      enableSetState: ['', Validators.required],
      enableAddText: ['', Validators.required],
      enableEditTitle: ['', Validators.required],
      maxVideos: ['', Validators.required],
      maxAudios: ['', Validators.required],
      maxImages: ['', Validators.required],
      priority: ['', Validators.required]
    });
  }
  guardar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.categoryService.updateCategory(this.category).subscribe(
      result => {
        if (result.status === 'success') {
          this.router.navigate(['../../list'], {relativeTo: this.route});
        } else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  ngAfterViewInit() {
    this.first.nativeElement.focus();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(p => {
      this.categoryService.getCategory(atob(p.get('id'))).subscribe(
        result => {
          if (result.status === 'success') {
            this.category = result.data;
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
}
