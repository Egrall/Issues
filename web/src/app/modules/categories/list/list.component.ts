import { Component, OnInit } from '@angular/core';
import { fadeLateral } from '../animations/fadeLateral';
import { Router, ActivatedRoute } from '@angular/router';
import {ConfirmationService, Message} from 'primeng/primeng';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  providers: [ConfirmationService, CategoryService],
  animations: [fadeLateral]
})

export class ListComponent implements OnInit {
  public msgs: Message[] = [];
  public categories = [];
  constructor(
    private categoryService: CategoryService,
    private confirmation: ConfirmationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  getPriority(category) {
    switch (category.priority) {
      case 'low': return 'Prioridad Baja';
      case 'medium': return 'Prioridad Media';
      case 'high': return 'Prioridad Alta';
    }
  }

  eliminar(category) {
    this.confirmation.confirm({
      message: '¿Desea eliminar esta categoría de forma permanente?',
      header: 'Confirmación',
      icon: 'fa fa-trash',
      accept: () => {
        this.categoryService.deleteCategory(category.id).subscribe(
          result => {
            if (result.status === 'success') {
              const i = this.categories.indexOf(category);
              this.categories.splice(i, 1);
              this.msgs = [{severity: 'error', summary: 'Confirmado', detail: 'Categoría eliminada'}];
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      },
      reject: () => {
          this.msgs = [{severity: 'info', summary: 'Rechazado', detail: 'No ocurrieron cambios'}];
      }
    });
  }
  navigateb64(id, url) {
    switch (url) {
      case 'config':
        this.router.navigate(['../config', btoa(id)], {relativeTo: this.route});
        break;
      case 'edit':
        this.router.navigate(['../edit', btoa(id)], {relativeTo: this.route});
        break;
    }
  }
  ngOnInit() {
    this.route.parent.parent.parent.paramMap.subscribe(p => {
      this.categoryService.getCategories(atob(p.get('id'))).subscribe(
        result => {
          if (result.status === 'success') {
            this.categories = result.data;
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
}
