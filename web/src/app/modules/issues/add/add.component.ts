import { MenuItem, MenuModule} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import { fadeLateral } from '../animations/fadeLateral';
import { Issue } from '../../../models/issue.model';
import { Category } from '../../../models/category.model';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CategoryService } from '../../../services/category.service';
import { Status } from '../../../models/status.enum';
import { IssuesService } from '../../../services/issues.service';
@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  animations: [fadeLateral],
  providers: [CategoryService]
})

export class AddComponent implements OnInit {
  public selectedCategory: Category;
  public categories: Category[];
  public issue: Issue;
  constructor(
    private issueService: IssuesService,
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.issue = new Issue(null, null, null, null, null, null, Status.open, null);
  }

  guardar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.issue.category = this.selectedCategory.id;
    this.issue.priority = this.selectedCategory.priority;
    this.issueService.createIssue(this.issue).subscribe(
      result => {
        if (result.status === 'success') {
          this.router.navigate(['../list'], {relativeTo: this.route});
        } else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  ngOnInit() {
    this.route.parent.parent.parent.paramMap.subscribe(
      p => {
        this.issue.project = atob(p.get('id'));
        this.issue.user = localStorage.getItem('currentID');
        this.categoryService.getCategories(atob(p.get('id'))).subscribe(
          result => {
            if (result.status === 'success') {
              this.categories = result.data;
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      }
    );
  }
}
