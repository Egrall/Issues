import { MenuItem } from 'primeng/primeng';
import { fadeLateral } from './../animations/fadeLateral';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  animations: [fadeLateral],
  styleUrls: ['./edit.component.css']
})

export class EditComponent implements OnInit {
  items: MenuItem[];
  issue: any;
  constructor() {
    this.items = [
      {label: 'Imagen', icon: 'fa-refresh'},
      {label: 'Usuario', icon: 'fa-close'},
      {label: 'Video', icon: 'fa-link'},
      {label: 'Ubicación', icon: 'fa-paint-brush'}
    ];
    this.issue = { issueId: 1, title: 'HardcodeTitle'};
  }
  ngOnInit() { }
}
