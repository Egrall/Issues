import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { IssuesRoutes } from './issues.routing';
import { ListComponent } from './list/list.component';
import { MainComponent } from './main/main.component';
import { NgModule } from '@angular/core';
import { SMapModule } from './../../components/map/map.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule, ButtonModule, InputTextModule, TabViewModule, RadioButtonModule, GMapModule,
  InputTextareaModule, CheckboxModule, FileUploadModule, PanelModule, ConfirmDialogModule, GrowlModule,
  TooltipModule, TabMenuModule, SplitButtonModule, MenuModule, DropdownModule} from 'primeng/primeng';
import { IssuesService } from '../../services/issues.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ButtonModule,
    InputTextModule,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    InputTextareaModule,
    FileUploadModule,
    PanelModule,
    ConfirmDialogModule,
    SMapModule,
    GrowlModule,
    TooltipModule,
    MenuModule,
    SMapModule,
    SplitButtonModule,
    IssuesRoutes,
    DropdownModule
  ],
  exports: [],
  declarations: [
    MainComponent,
    ListComponent,
    AddComponent,
    EditComponent
  ],
  providers: [IssuesService],
})
export class IssuessModule { }
