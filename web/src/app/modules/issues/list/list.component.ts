import { Component, OnInit } from '@angular/core';
import {ConfirmationService, Message} from 'primeng/primeng';
import { fadeLateral } from '../animations/fadeLateral';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { IssuesService } from '../../../services/issues.service';
@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  providers: [ConfirmationService],
  animations: [fadeLateral]
})

export class ListComponent implements OnInit {
  searchLocationText = '';
  msgs: Message[] = [];
  public issues = [];
  constructor(
    private issuesService: IssuesService,
    private confirmation: ConfirmationService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  eliminar(issue) {
    this.confirmation.confirm({
      message: '¿Desea eliminar esta incidencia de forma permanente?',
      header: 'Confirmacion',
      icon: 'fa fa-trash',
      accept: () => {
        this.issuesService.deleteIssue(issue.id).subscribe(
          result => {
            if (result.status === 'success') {
              const i = this.issues.indexOf(issue);
              this.issues.splice(i, 1);
              this.msgs = [{severity: 'error', summary: 'Confirmado', detail: 'Incidencia eliminada'}];
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      },
      reject: () => {
          this.msgs = [{severity: 'info', summary: 'Rechazado', detail: 'No ocurrieron cambios'}];
      }
    });
  }
  navigateb64(id, url) {
    switch (url) {
      case 'edit':
        this.router.navigate(['../edit', btoa(id)], {relativeTo: this.route});
        break;
    }
  }
  getPriority(issue) {
    switch (issue.priority) {
      case 'low': return 'Prioridad Baja';
      case 'medium': return 'Prioridad Media';
      case 'high': return 'Prioridad Alta';
    }
  }
  getStatus(issue) {
    switch (issue.status) {
      case 'open': return 'Pendiente';
      case 'resolved': return 'Resuelta';
      case 'rejected': return 'Rechazada';
    }
  }
  ngOnInit() {
    this.route.parent.parent.parent.paramMap.subscribe(
      p => {
        this.issuesService.getIssues(atob(p.get('id'))).subscribe(
          result => {
            if (result.status === 'success') {
              this.issues = result.data;
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      }
    );
  }
}
