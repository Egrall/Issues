import { ProjectUser } from './../../../models/projectuser.model';
import { ProjectsService } from './../../../services/projects.service';
import { GlobalService } from './../../../services/global.service';
import { Component, OnInit, AfterViewInit, NgZone, ViewChild} from '@angular/core';
import {ConfirmationService, Message} from 'primeng/primeng';
import { fadeLateral } from '../animations/fadeLateral';
import { Project } from '../../../models/project.model';
import { Router, ActivatedRoute } from '@angular/router';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { Visibility } from '../../../models/visibility.enum';
import { MenuItem } from 'primeng/components/common/menuitem';
import { Category } from '../../../models/category.model';
import { Priority } from '../../../models/priority.enum';
import { CategoryService } from '../../../services/category.service';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  providers: [ConfirmationService, CategoryService],
  animations: [fadeLateral]
})

export class AddComponent implements OnInit, AfterViewInit {
  @ViewChild('map') map;
  @ViewChild('first') first;
  title = 'Agregar';
  activeIndex = 0;
  tabIndex = 0;
  last = false;
  steps: MenuItem[];
  public addProject: FormGroup;
  public addCategory: FormGroup;
  private project: Project;
  private category: Category;
  private url: string;
  private files: File[];
  public urlPicture: string;
  cPM = true;
  constructor(
    private fb: FormBuilder,
    private confirmation: ConfirmationService,
    private GLOBAL: GlobalService,
    private projectsService: ProjectsService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.project = new Project(null, null, null, null, null, null, Visibility.private, null);
    this.category = new Category(null, null, null, Priority.low, null, 5, 5, 5, true, true, true, true, true, null);
    this.url = GLOBAL.url;
    this.addProject = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      visibility: ['', Validators.required],
    });
    this.addCategory = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      enableSetPriorities: ['', Validators.required],
      enableSetGeolocation: ['', Validators.required],
      enableSetState: ['', Validators.required],
      enableAddText: ['', Validators.required],
      enableEditTitle: ['', Validators.required],
    });
    this.steps = [{label: 'Principal'}, {label: 'Categoria'}];
  }
  checkValid() {
    return !(this.addProject.valid && this.project.latitude != null && this.project.longitude != null);
  }
  getCurrentTab(tab) {
    this.tabIndex = tab.index;
    this.checkLast();
  }
  guardar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.project.user = this.GLOBAL.user.id;
    this.projectsService.createProject(this.project).subscribe(
      result => {
        if (result.status == 'success') {
            this.category.user = this.GLOBAL.user.id;
            this.category.project = result.data.id;
            this.projectsService.assignUser(new ProjectUser(null, this.project.user, result.data.id, true, true, true)).subscribe(
              result => {
                if (result.status == 'success') {
                  this.categoryService.createCategory(this.category).subscribe(
                    result => {
                      if (result.status == 'success') {
                        if (this.files) {
                          this.projectsService.makeFileRequest(this.url + '/endpoint/projects/uploadImage/' +
                          this.category.project, [], this.files, 'picture')
                          .then((result: any) => {
                            console.log(result);
                            this.router.navigate(['../list'], {relativeTo: this.route});
                          });
                        } else {
                          this.router.navigate(['../list'], {relativeTo: this.route});
                        }
                      } else {
                        console.log(<any>result);
                      }
                    },
                    error => {
                      console.log(<any>error);
                    }
                  );
                }else {
                  console.log(<any>result);
                }
              },
              error => {
                console.log(<any>error);
              }
            );
        }else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  fileChange(event) {
    if (event.target.files.length > 0) {
      this.files = event.target.files;
      console.log(this.files);
      this.readThis(event.target);
    }
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    myReader.onloadend = this.changeUrlPicture.bind(this);
    myReader.readAsDataURL(file);
  }

  changeUrlPicture(e) {
    this.urlPicture = e.target.result;
  }

  checkLast() {
    if (this.tabIndex === 1) {
      this.last = true;
    }else {
      this.last = false;
    }
  }

  Previous() {
    this.activeIndex = 0;
  }
  Next(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.cPM = false;
    this.activeIndex = 1;
  }

  ngAfterViewInit() {
    this.first.nativeElement.focus();
  }

  ngOnInit() {
  }
}
