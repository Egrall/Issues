import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { fadeLateral } from '../animations/fadeLateral';
import { Project } from '../../../models/project.model';
import { Visibility } from '../../../models/visibility.enum';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'config',
  templateUrl: './config.component.html',
  animations: [fadeLateral]
})

export class ConfigComponent implements OnInit {
  public urlPicture: string;
  public sections: MenuItem[];
  public project: Project;
  constructor(
    private route: ActivatedRoute
  ) {
    this.project = new Project('1', 'ProjectHardcode', 'DescriptionHardcode', null, null, null, Visibility.private, null);
  }

  getVisibility(visibility) {
    switch (visibility) {
      case 'public':
          return 'PÚBLICO';
      case 'private':
          return 'PRIVADO';
      default:
          return 'UNDEFINED';
    }
  }

  ngOnInit() {
    this.sections = [
      {label: 'Categorias', icon: 'fa-bar-chart', routerLink: ['categories']},
      {label: 'Usuarios', icon: 'fa-user', routerLink: ['users']},
      {label: 'Incidencias', icon: 'fa-book', routerLink: ['issues']}
    ];
    this.project = this.route.snapshot.data['configProject'].data;
    this.urlPicture = 'http://localhost:3000/endpoint/projects/getImage/' + this.project.picture;
  }
}
