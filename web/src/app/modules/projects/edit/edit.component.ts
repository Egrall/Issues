import { ProjectsService } from './../../../services/projects.service';
import { GlobalService } from './../../../services/global.service';
import { Component, OnInit, AfterViewInit, NgZone, ViewChild} from '@angular/core';
import {ConfirmationService, Message} from 'primeng/primeng';
import { fadeLateral } from '../animations/fadeLateral';
import { Project } from '../../../models/project.model';
import { Router, ActivatedRoute } from '@angular/router';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { Visibility } from '../../../models/visibility.enum';

@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  providers: [ConfirmationService],
  animations: [fadeLateral]
})

export class EditComponent implements OnInit, AfterViewInit {
  @ViewChild('map') map;
  @ViewChild('first') first;
  title = 'Editar';
  tabIndex = 0;
  last = false;
  cPM = false;
  private addProject: FormGroup;
  private project: Project;
  private url: string;
  public urlPicture: string;
  constructor(
    private fb: FormBuilder,
    private confirmation: ConfirmationService,
    private GLOBAL: GlobalService,
    private projectsService: ProjectsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.project = new Project(null, null, null, null, null, null, Visibility.private, null);
    this.url = GLOBAL.url;
    this.addProject = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      visibility: ['', Validators.required],
    });
  }
  checkValid() {
    return !(this.addProject.valid && this.project.latitude != null && this.project.longitude != null);
  }
  getCurrentTab(tab) {
    this.tabIndex = tab.index;
    this.checkLast();
  }
  guardar(event) {
    if (this.last) {
      if (event.type == 'keypress' && event.keyCode != 13) {
        return;
      } else if (event.type == 'click') {
          event.preventDefault();
      }
      this.project.user = this.GLOBAL.user.id;
      this.projectsService.updateProject(this.project).subscribe(
        result => {
          if (result.status == 'success') {
            this.router.navigate(['../../list'], {relativeTo: this.route});
          }else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    }
  }

  fileChange(event) {
    if (event.target.files.length > 0) {
      this.projectsService.makeFileRequest(this.url + '/endpoint/projects/uploadImage/' +
      this.project.id, [], event.target.files, 'picture')
      .then((result: any) => {
        this.project.picture = result.image;
        this.urlPicture = this.url + '/endpoint/projects/getImage/' + this.project.picture;
      });
    }
  }

  checkLast() {
    if (this.tabIndex === 1) {
      this.last = true;
    }else {
      this.last = false;
    }
  }

  Previous() {
    if (this.tabIndex !== 0) {
      this.tabIndex--;
      this.checkLast();
    }
  }
  Next() {
    if (this.tabIndex !== 1 ) {
      this.tabIndex++;
      this.checkLast();
    }
  }

  ngAfterViewInit() {
    this.first.nativeElement.focus();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      p => {
        this.projectsService.getProject(atob(p.get('id'))).subscribe(
          result => {
            if (result.status == 'success') {
              this.project = result.data;
              this.urlPicture = this.url + '/endpoint/projects/getImage/' + this.project.picture;
            }else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      }
    );
  }
}
