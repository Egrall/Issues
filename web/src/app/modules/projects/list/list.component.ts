import { Project } from './../../../models/project.model';
import { Component, OnInit } from '@angular/core';
import { fadeLateral } from '../animations/fadeLateral';
import {ConfirmationService, Message} from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectUser } from '../../../models/projectuser.model';
import { ProjectsService } from '../../../services/projects.service';
@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  providers: [ConfirmationService],
  animations: [fadeLateral]
})

export class ListComponent implements OnInit {
  msgs: Message[] = [];
  private projects: any[];
  constructor(
    private projectService: ProjectsService,
    private confirmation: ConfirmationService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  navigateb64(id, url) {
    switch (url) {
      case 'config':
        this.router.navigate(['../config', btoa(id)], {relativeTo: this.route});
        break;
      case 'edit':
        this.router.navigate(['../edit', btoa(id)], {relativeTo: this.route});
        break;
    }
  }
  eliminar(project) {
    this.confirmation.confirm({
      message: '¿Desea eliminar este proyecto de forma permanente?',
      header: 'Confirmación',
      icon: 'fa fa-trash',
      accept: () => {
          this.projectService.deleteProject(project.project.id).subscribe(
            result => {
              if (result.status === 'success') {
                const i = this.projects.indexOf(project);
                this.projects.splice(i, 1);
                this.msgs = [{severity: 'error', summary: 'Confirmado', detail: 'Proyecto eliminado'}];
              } else {
                console.log(result);
              }
            }, error => {
              console.log(<any>error);
            }
          );
      },
      reject: () => {
          this.msgs = [{severity: 'info', summary: 'Rechazado', detail: 'No ocurrieron cambios'}];
      }
    });
  }
  getRole(project) {
    if (project.isAdmin) {
      return 'Administrador';
    }
    if (project.isManager) {
      return 'Gestor';
    }
    if (project.isUser) {
      return 'Usuario';
    }
  }
  ngOnInit() {
    if (this.route.snapshot.data['projects'].status == 'success') {
      this.projects = this.route.snapshot.data['projects'].data;
    }
  }
}
