import { ProjectsService } from './../../../services/projects.service';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'projects',
  templateUrl: './main.component.html'
})

export class MainComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private projectsService: ProjectsService
  ) {
  }
  ngOnInit() {
  }
}
