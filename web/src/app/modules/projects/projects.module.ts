import { GlobalService } from './../../services/global.service';
import { ProjectsResolve } from './../../services/projects.resolve';
import { EditComponent } from './edit/edit.component';
import { MainComponent } from './main/main.component';
import { SMapModule } from './../../components/map/map.module';
import { AddComponent } from './add/add.component';
import { ProjectsRoutes } from './projects.routing';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DataTableModule, ButtonModule, InputTextModule, TabViewModule, RadioButtonModule, GMapModule,
InputTextareaModule, CheckboxModule, FileUploadModule, PanelModule, ConfirmDialogModule, GrowlModule,
TooltipModule, ToggleButtonModule, ToggleButton, StepsModule, TabMenuModule} from 'primeng/primeng';
import { ProjectsService } from '../../services/projects.service';
import { ConfigComponent } from './config/config.component';
import { ConfigProjectResolve } from '../../services/configProject.resolve';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ButtonModule,
    InputTextModule,
    ProjectsRoutes,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    InputTextareaModule,
    FileUploadModule,
    PanelModule,
    ConfirmDialogModule,
    SMapModule,
    GrowlModule,
    TooltipModule,
    ReactiveFormsModule,
    ToggleButtonModule,
    StepsModule,
    TabMenuModule
  ],
  declarations: [
    ListComponent,
    AddComponent,
    MainComponent,
    EditComponent,
    ConfigComponent
  ],
  exports: [MainComponent],
  providers: [ProjectsService, ProjectsResolve, ConfigProjectResolve]
})
export class ProjectsModule { }
