import { UsersModule } from './../users/users.module';
import { IssuessModule } from './../issues/issues.module';
import { ProjectsResolve } from './../../services/projects.resolve';
import { EditComponent } from './edit/edit.component';
import { MainComponent } from './main/main.component';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CategoriesModule } from './../categories/categories.module';
import { ConfigComponent } from './config/config.component';
import { ConfigProjectResolve } from '../../services/configProject.resolve';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'list' },
      { path: 'list', component: ListComponent, resolve: {projects: ProjectsResolve} },
      { path: 'add', component: AddComponent },
      { path: 'edit/:id', component: EditComponent },
      { path: 'config/:id',
        resolve: { configProject: ConfigProjectResolve },
        component: ConfigComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'categories' },
          { path: 'issues', loadChildren: getIssuesModule },
          { path: 'categories', loadChildren: getCategoriesModule },
          { path: 'users', loadChildren: getUsersModule }
        ]
      }
    ]
  }
];

export function getIssuesModule() {
  return  IssuessModule;
}
export function getUsersModule() {
  return  UsersModule;
}
export function getCategoriesModule() {
  return CategoriesModule;
}

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProjectsRoutes {}
