import { User } from './../../../models/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { validarNumeros, passwordConfirming, emailValidator} from './../../../validators/validator';
import { fadeLateral } from '../animations/fadeLateral';
import { UsersService } from './../../../services/users.service';
import { AuthService } from './../../../services/auth.service';
import { ProjectsService } from '../../../services/projects.service';
import { ProjectUser } from '../../../models/projectuser.model';
@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  animations: [fadeLateral],
  providers: [ProjectsService]
})

export class AddComponent implements OnInit {
  private projectUser: ProjectUser;
  public user: User;
  public repeatP: string;
  public registerForm: FormGroup;
  public emailUnique: Boolean;
  public usernameUnique: Boolean;
  constructor(
    private projectService: ProjectsService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private userService: UsersService,
    private fb: FormBuilder
  ) {
    this.emailUnique = false;
    this.usernameUnique = false;
    this.user = new User(null, null, null, null, null, null, null, null, null, null);
    this.projectUser = new ProjectUser(null, null, null, false, false, true);
    this.registerForm = this.fb.group({
      lastName: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, emailValidator] ],
      username: ['', Validators.required],
      passwords: this.fb.group({
        password: ['', Validators.required],
        repeatP: ['', Validators.required]
      }, {validator: passwordConfirming})
    });
  }

  resetEmailUnique() {
    this.emailUnique = false;
  }
  resetUsernameUnique() {
    this.usernameUnique = false;
  }
  registrar(event) {
    if (event.type == 'keypress' && event.keyCode != 13) {
      return;
    } else if (event.type == 'click') {
        event.preventDefault();
    }
    this.userService.validateUsername(this.user.username).subscribe(
      result => {
        if (result.status == 'success') {
          if (result.data) {
            this.usernameUnique = result.data.username;
          }
        } else {
          console.log(<any>result);
        }
        this.userService.validateEmail(this.user.email).subscribe(
          result => {
            if (result.status == 'success') {
              if (result.data) {
                this.emailUnique = result.data.email;
              }
            } else {
              console.log(<any>result);
            }
            this.auth.register(this.user).subscribe(
              result => {
                if (result.status == 'success') {
                  this.projectUser.user = result.data.id;
                  this.projectService.assignUser(this.projectUser).subscribe(
                    result => {
                      if (result.status === 'success') {
                        this.router.navigate(['../list'], {relativeTo: this.route});
                      } else {
                        console.log(<any>result);
                      }
                    },
                    error => {
                      console.log(<any>error);
                    }
                  );
                }else {
                  console.log(<any>result);
                }
              },
              error => {
                console.log(<any>error);
              }
            );
          },
          error => {
            console.log(<any>error);
          }
        );
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  ngOnInit() {
    this.route.parent.parent.parent.paramMap.subscribe(p => {
      this.projectUser.project = atob(p.get('id'));
    });
  }
}
