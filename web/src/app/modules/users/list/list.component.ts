import { Component, OnInit, ViewChild } from '@angular/core';
import {ConfirmationService, Message} from 'primeng/primeng';
import { fadeLateral } from '../animations/fadeLateral';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../../services/users.service';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ConfirmationService],
  animations: [fadeLateral]
})

export class ListComponent implements OnInit {
  @ViewChild('searchBannedUsers') searchBannedUsers;
  searchLocationText = '';
  msgs: Message[] = [];
  private currentProject: string;
  public activeUsers = [];
  public bannedUsers = [];
  public user = { id: null};
  public projectUser;
  private tempUser = {};
  public banTerm = new Subject<string>();
  constructor(
    private userService: UsersService,
    private confirmation: ConfirmationService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }
  searchUser(event, overlay, username) {
    if (username !== '') {
      this.userService.searchUser(this.banTerm).subscribe(
        result => {
          if (result.status === 'success') {
            if (result.data) {
              this.projectUser = result.data;
              overlay.show(event);
            } else {
              this.projectUser = null;
              overlay.hide();
            }
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
      this.banTerm.next(username);
    }
  }
  getRoles(user) {
    const result = [];
    if (user.isAdmin) {
      result.push('Admin');
    }
    if (user.isManager) {
      result.push('Gestor');
    }
    if (user.isUser) {
      result.push('Usuario');
    }
    return result.join(', ');
  }
  eliminar(user) {
    this.confirmation.confirm({
      message: '¿Desea eliminar este usuario de forma permanente?',
      header: 'Confirmación',
      icon: 'fa fa-trash',
      accept: () => {
        this.userService.deleteProjectUser(user.id).subscribe(
          result => {
            if (result.status === 'success') {
              const i = this.activeUsers.indexOf(user);
              this.activeUsers.splice(i, 1);
              this.msgs = [{severity: 'error', summary: 'Confirmado', detail: 'Usuario eliminado'}];
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      },
      reject: () => {
          this.msgs = [{severity: 'info', summary: 'Rechazado', detail: 'No ocurrieron cambios'}];
      }
    });
  }
  navigateb64(id, url) {
    switch (url) {
      case 'edit':
        this.router.navigate(['../edit', btoa(id)], {relativeTo: this.route});
        break;
    }
  }
  isAdmin() {
    if (this.activeUsers.length > 0) {
      const user = this.activeUsers.find(x => x.user.id === localStorage.getItem('currentID'));
      return user.isAdmin;
    }
    return false;
  }
  banUser(overlay) {

    overlay.hide();
  }
  assignUser(overlay) {
    if (this.activeUsers.find(x => x.user.id === this.projectUser.id) === undefined) {
      this.userService.assignUser({
        project: this.currentProject,
        user: this.projectUser.id,
        isAdmin: false,
        isManager: false,
        isUser: true
      }).subscribe(
        result => {
          if (result.status === 'success') {
            this.activeUsers.push({
              project: this.currentProject,
              user: this.projectUser,
              isAdmin: false,
              isManager: false,
              isUser: true
            });
          } else {
            console.log(<any>result);
          }
        },
        error => {
          console.log(<any>error);
        }
      );
    }
    overlay.hide();
  }
  editUser(event, user, overlay) {
    this.tempUser = user;
    Object.assign(this.user, user);
    overlay.show(event);
  }
  saveUser() {
    let updatedUser = {user: null};
    Object.assign(updatedUser, this.user);
    updatedUser.user = updatedUser.user.id;
    this.userService.updateProjectUser(updatedUser).subscribe(
      result => {
        if (result.status === 'success') {
          const i = this.activeUsers.indexOf(this.activeUsers.find(x => x.id === this.user.id));
          this.activeUsers[i] = this.user;
        } else {
          console.log(<any>result);
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  ngOnInit() {
    this.route.parent.parent.parent.paramMap.subscribe(
      p => {
        this.currentProject = atob(p.get('id'));
        this.userService.getProjectUsers(atob(p.get('id'))).subscribe(
          result => {
            if (result.status === 'success') {
              this.activeUsers = result.data;
              this.userService.getBannedUsers(atob(p.get('id'))).subscribe(
                result => {
                  this.bannedUsers = result.data;
                },
                error => {
                  console.log(<any>error);
                }
              );
            } else {
              console.log(<any>result);
            }
          },
          error => {
            console.log(<any>error);
          }
        );
      }
    );
  }
}
