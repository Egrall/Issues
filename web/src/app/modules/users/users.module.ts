import { AddComponent } from './add/add.component';
import { UsersRoutes } from './users.routing';
import { ListComponent } from './list/list.component';
import { MainComponent } from './main/main.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SMapModule } from './../../components/map/map.module';
import { CommonModule } from '@angular/common';
import { DataTableModule, ButtonModule, InputTextModule, TabViewModule, RadioButtonModule, GMapModule,
  InputTextareaModule, CheckboxModule, OverlayPanelModule, PanelModule, ConfirmDialogModule, GrowlModule,
  TooltipModule, TabMenuModule, SplitButtonModule, MenuModule, DataListModule, ToggleButtonModule} from 'primeng/primeng';
import { UsersService } from '../../services/users.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    ButtonModule,
    InputTextModule,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    InputTextareaModule,
    OverlayPanelModule,
    PanelModule,
    ConfirmDialogModule,
    SMapModule,
    GrowlModule,
    TooltipModule,
    MenuModule,
    SMapModule,
    SplitButtonModule,
    DataListModule,
    UsersRoutes,
    ToggleButtonModule
  ],
  exports: [],
  declarations: [
    MainComponent,
    ListComponent,
    AddComponent
  ],
  providers: [UsersService],
})
export class UsersModule { }
