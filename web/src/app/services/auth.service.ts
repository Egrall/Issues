import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  url: string;
  constructor(private http: HttpClient, private GLOBAL: GlobalService) {
    this.url = this.GLOBAL.url;
  }
  register(usertoRegister) {
    const params = JSON.stringify(usertoRegister);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.url + '/users', params, {headers: headers});
  }
  login(usertoLogin) {
    const params = JSON.stringify(usertoLogin);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<any>(this.url + '/endpoint/users/login', params, {headers: headers});
  }
  isLogged() {
    return localStorage.getItem('token') != null && localStorage.getItem('currentID') != null;
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentID');
  }
}
