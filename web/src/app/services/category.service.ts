import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';

@Injectable()
export class CategoryService {
  private url: string;
  constructor(
    private httpClient: HttpClient,
    private GLOBAL: GlobalService
  ) {
    this.url = GLOBAL.url;
  }

  createCategory(category) {
    const params = JSON.stringify(category);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/categories', params, {headers: headers});
  }

  getCategory(id) {
    return this.httpClient.get<any>(this.url + '/categories/' + id);
  }

  getCategories(project) {
    return this.httpClient.get<any>(this.url + '/endpoint/categories/categoriesByProject/' + project);
  }

  updateCategory(category) {
    const params = JSON.stringify(category);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.put<any>(this.url + '/categories/' + category.id, params, {headers: headers});
  }

  deleteCategory(id) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.delete<any>(this.url + '/endpoint/categories/deleteCategories/' + id, {headers: headers});
  }

}
