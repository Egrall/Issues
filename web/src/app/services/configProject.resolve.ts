import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';
import { ProjectsService } from './projects.service';

@Injectable()
export class ConfigProjectResolve implements Resolve<any> {
  constructor(
    private projectService: ProjectsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.projectService.getProject(atob(route.params.id));
  }
}
