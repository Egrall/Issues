import { AuthService } from './auth.service';
import { User } from './../models/user.model';
import { UsersService } from './users.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';

@Injectable()
export class CurrentUserResolve implements Resolve<any> {
  constructor(
    private userService: UsersService,
    private auth: AuthService,
    public GLOBAL: GlobalService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (this.auth.isLogged()) {
      if (route.url[0].path == 'login') {
        this.router.navigate(['/dashboard']);
        return;
      }
      return this.userService.getCurrentUser();
    }
  }
}
