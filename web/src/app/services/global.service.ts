import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class GlobalService {
  public user: User;
  public url = 'http://localhost:3000';
  constructor(private httpClient: HttpClient) {
  }
}
