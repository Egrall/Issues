import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';

@Injectable()
export class IssuesService {
  public url: string;
  constructor(private httpClient: HttpClient, private GLOBAL: GlobalService) {
    this.url = GLOBAL.url;
  }
  getIssues(project) {
    return this.httpClient.get<any>(this.url + '/endpoint/issues/issuesByProject/' + project);
  }
  createIssue(issue) {
    const params = JSON.stringify(issue);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/issues', params, {headers: headers});
  }
  deleteIssue(id) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.delete<any>(this.url + '/issues/' + id, {headers: headers});
  }
}
