import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute} from '@angular/router';
import { AuthService} from './auth.service';

@Injectable()
export class LoggedGuard implements CanActivate {
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  canActivate() {
    const logged = this.auth.isLogged();
    if (logged) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
