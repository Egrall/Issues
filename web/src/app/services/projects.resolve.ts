import { ProjectsService } from './projects.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ProjectsResolve implements Resolve<any> {
  constructor(
    private projects: ProjectsService
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.projects.getProjects();
  }
}
