import { UsersService } from './users.service';
import { Project } from './../models/project.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { GlobalService } from './global.service';

@Injectable()
export class ProjectsService {
  public project: Project;
  private url: string;
  constructor(
    private httpClient: HttpClient,
    private GLOBAL: GlobalService,
    private userService: UsersService
  ) {
    this.url = GLOBAL.url;
  }
  getProjects() {
    return this.httpClient.get<any>(this.url + '/endpoint/projectusers/getProjects/' + localStorage.getItem('currentID'));
  }
  getProject(id) {
    return this.httpClient.get<any>(this.url + '/projects/' + id);
  }
  createProject(project) {
    const params = JSON.stringify(project);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/projects', params, {headers: headers});
  }
  assignUser(projectUser) {
    const params = JSON.stringify(projectUser);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/projectusers', params, {headers: headers});
  }
  getUsers(project) {
    return this.httpClient.get<any>(this.url + '/endpoint/projectusers/getUsers/' + project);
  }
  updateProject(project) {
    const params = JSON.stringify(project);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.put<any>(this.url + '/projects/' + project.id, params, {headers: headers});
  }
  deleteProject(id) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.delete<any>(this.url + '/projects/' + id, {headers: headers});
  }
  makeFileRequest(url: string, params: Array<string>, files: Array<File>, name: string) {
    if (files.length > 0) {
      return new Promise(function(resolve, reject){
        const formData: any = new FormData();
        const xhr = new XMLHttpRequest();

        for (let i = 0; i < files.length; i++) {
          formData.append(name, files[i], files[i].name);
        }

        xhr.onreadystatechange = function(){
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              resolve(JSON.parse(xhr.response));
            }else {
              resolve(xhr.response);
            }
          }
        };
        xhr.open('POST', url, true);
        // xhr.setRequestHeader('Authorization', token);
        xhr.send(formData);
      });
    } else {
      console.log('Ningun archivo se ha enviado al servicio');
    }
  }
}
