import { User } from './../models/user.model';
import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class UsersService {
  private url: string;
  public user: User;
  constructor(private httpClient: HttpClient, private GLOBAL: GlobalService) {
    this.url = this.GLOBAL.url;
  }

  validateUsername(username) {
    const params = JSON.stringify({username: username});
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/endpoint/users/validateUsername', params, {headers: headers});
  }
  validateEmail(email) {
    const params = JSON.stringify({email: email});
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/endpoint/users/validateEmail', params, {headers: headers});
  }
  getUser(id) {
    return this.httpClient.get<any>(this.url + '/users/' + id);
  }

  getCurrentUser() {
    return this.getUser(localStorage.getItem('currentID'));
  }
  getBannedUsers(project) {
    return this.httpClient.get<any>(this.url + '/endpoint/bans/byProject/' + project);
  }
  searchUser(terms: Observable<string>) {
    return terms.debounceTime(400).distinctUntilChanged().switchMap(term => this.getUserByUsername(term));
  }
  banUser(userBan) {
    const params = JSON.stringify(userBan);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/bans', params, {headers: headers});
  }
  assignUser(projectUser) {
    const params = JSON.stringify(projectUser);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post<any>(this.url + '/projectusers', params, {headers: headers});
  }
  getUserByUsername(username) {
    return this.httpClient.get<any>(this.url + '/endpoint/users/byUsername/' + username);
  }
  getProjectUsers(project) {
    return this.httpClient.get<any>(this.url + '/endpoint/projectusers/getUsers/' + project);
  }
  updateProjectUser(projectUser) {
    const params = JSON.stringify(projectUser);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.put<any>(this.url + '/projectusers/' + projectUser.id, params, {headers: headers});
  }
  deleteProjectUser(id) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.delete<any>(this.url + '/projectusers/' + id, {headers: headers});
  }
  updateUser(usertoUpdate) {
    delete usertoUpdate.password;
    const params = JSON.stringify(usertoUpdate);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.put<any>(this.url + '/users/' + usertoUpdate.id, params, {headers: headers});
  }
  makeFileRequest(url: string, params: Array<string>, files: Array<File>, name: string) {
    if (files.length > 0) {
      return new Promise(function(resolve, reject){
        const formData: any = new FormData();
        const xhr = new XMLHttpRequest();

        for (let i = 0; i < files.length; i++) {
          formData.append(name, files[i], files[i].name);
        }

        xhr.onreadystatechange = function(){
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              resolve(JSON.parse(xhr.response));
            }else {
              resolve(xhr.response);
            }
          }
        };
        xhr.open('POST', url, true);
        // xhr.setRequestHeader('Authorization', token);
        xhr.send(formData);
      });
    } else {
      console.log('Ningun archivo se ha enviado al servicio');
    }
  }

}
