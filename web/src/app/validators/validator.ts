import {FormControl, FormGroup} from '@angular/forms';

export function emailValidator(control: FormControl): {[key: string]: any} {
  const emailRegexp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}
export function passwordConfirming(c: FormGroup): {[key: string]: any} {
  if (c.get('repeatP').value && (c.get('password').value !== c.get('repeatP').value)) {
      return {invalidConfirmPassword: true};
  }
}
export function validarNumeros(event) {
  let regex;
  regex = /^([0-9])*$/;
  // regex  = /^\d+(?:\.\d)$/;
  return regex.test(String.fromCharCode(event));
};

